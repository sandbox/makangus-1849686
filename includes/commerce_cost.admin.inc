<?php

/**
 * @file
 * Administrative callbacks and form builder functions for Commerce Cost.
 */

/**
 * Commerce Cost admin form.
 */
function commerce_cost_admin_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['product_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable cost management for these product types'),
    '#description' => t('Note that disabling cost management removes the Cost field from the product type, deleting any existing cost data for that product type.'),
  );

  // Create a checkbox for each product type, set with the current cost-
  // enabled state.
  foreach (commerce_product_types() as $type => $product_type) {
    $instance[$type] = field_info_instance('commerce_product', 'commerce_cost', $type);
    $enabled[$type] = (!empty($instance[$type]));

    $form['product_types'][$type] = array(
      '#type' => 'checkbox',
      '#default_value' => $enabled[$type],
      '#title' => t('@name (@machine_name)', array('@name' => $product_type['name'], '@machine_name' => $type)),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Add or remove the Cost field from product types.
 */
function commerce_cost_admin_form_submit($form, &$form_state) {
  foreach ($form_state['values']['product_types'] as $type => $enable) {
    $instance = field_info_instance('commerce_product', 'commerce_cost', $type);
    $currently_enabled = commerce_cost_product_type_enabled($type);
    // If they want us to enable it and it doesn't currently exist, do the work.
    if ($enable && !$currently_enabled) {
      commerce_cost_create_instance('commerce_cost', 'commerce_price', TRUE, 'commerce_product', $type, t('Cost'));
      drupal_set_message(t('Cost management has been enabled on the %type product type', array('%type' => $type)));
    }
    elseif (!$enable && $currently_enabled) {
      // Remove the instance.
      field_delete_instance($instance);
      drupal_set_message(t('Cost management has been disabled on the %type product type', array('%type' => $type)));
    }

  }
}

/**
 * Creates a required instance of a cost field on the specified bundle.
 */
function commerce_cost_create_instance($field_name, $field_type, $required, $entity_type, $bundle, $label, $description = NULL, $weight = 0) {
  // Look for or add the specified cost field to the requested entity bundle.
  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  if (empty($field)) {
    $field = array(
      'field_name' => $field_name,
      'type' => $field_type,
      'cardinality' => 1,
      'entity_types' => array($entity_type),
      'translatable' => FALSE,
      'locked' => FALSE,
    );
    if ($field_type == 'list_boolean') {
      $field['settings'] = array(
        'allowed_values' => array(0, 1),
        'allowed_values_function' => '',
      );
    }
    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $label,
      'required' => $required,
      'settings' => array(),
      'display' => array(),
      'description' => $description,
      'default_value' => array(array('value' => "0")),
    );

    if ($field_type == 'list_boolean') {
      $instance['widget'] = array(
        'type' => 'options_onoff',
        'settings' => array(
          'display_label' => TRUE,
        ),
      );
    }

    $entity_info = entity_get_info($entity_type);

    // Spoof the default view mode so its display type is set.
    $entity_info['view modes']['default'] = array();

    field_create_instance($instance);
  }
}
